import processing.svg.*;

void saveSVG() {
  PGraphics canvas;
  canvas = createGraphics(width, height, SVG, EXPORT_NAME);
  canvas.beginDraw();
  canvas.background(WHITE);
  canvas.stroke(BLACK);
  canvas.noFill();
  canvas.strokeWeight(STROKE_WEIGHT);

  for (Circle c : circles) {
    canvas.ellipse(c.center.x, c.center.y, c.radius*2, c.radius*2);
  }

  canvas.endDraw();
  canvas.dispose();
  println("Image Saved.");
}