class ThresholdImg {
  ArrayList <PVector> filteredPositions;
  PImage source;
  float threshold;
  color stroke;

  ThresholdImg(PImage source, float threshold) {
    this.threshold = threshold;
    this.stroke = BLACK;
    this.source = source;
    this.source.resize(width, height);
    filteredPositions = initPositions(this.source, threshold);
  }

  void display() { // This is a very expensive function. Do not run in draw.
    stroke(stroke);

    for (PVector p : filteredPositions) {
      point(p.x, p.y);
    }
  }

  float getBrightness(int x, int y) {    
    return brightness(source.get(x, y));
  }

  boolean isValid(int x, int y) {
    boolean valid = getBrightness(x, y) < threshold;
    return valid;
  }

  ArrayList<PVector> initPositions (PImage source, float thresh) {
    ArrayList<PVector> filtered = new ArrayList<PVector>();
    source.loadPixels();

    for (int i = 0; i < source.pixels.length; i++) {
      int x = i % source.width;
      int y = i / source.width;
      float pixelBrightness = brightness(source.get(x, y));

      if (pixelBrightness < thresh) {
        filtered.add(new PVector(x, y));
      }
    }
    println("Found " + filtered.size() + " possible positions from image.");
    return filtered;
  }
}