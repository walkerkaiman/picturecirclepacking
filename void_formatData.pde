import java.util.*;

void formatData() {
  println();
  println("Purging unwanted data.");

  int purgedCircles = 0;

  for (int i = circles.size()-1; i > 0; i--) { // Check if circle is large enough to be seen.
    if (circles.get(i).radius <= 0) {
      circles.remove(i);
      purgedCircles++;
    }
  } 

  println("Unseeable Circles removed: " + purgedCircles);
  println(circles.size() + " total circles in image.");
  println();

  println("Sorting to reduce travel time.");
  Collections.sort(circles, new CircleComparator());
  println("Sorted.");
  println();
}