// USER CONFIGURABLE PARAMETERS
final String IMAGE_NAME = "1.jpg";
final String EXPORT_NAME = "packedCircles2.svg";
final float IMAGE_THRESHOLD = 127; // Brightness threshold for the image for determining possible circle location.
final float STROKE_WEIGHT = 1;
final int FORMAT_FREQUENCY = 5; // Cleans the data every X failures to find a valid location for a circle.
final int TIMEOUT_LIMIT = 3; // Number of times the program can fail to find a valid location for a circle.
final color WHITE = color(255);
final color BLACK = color(0);






// DO NOT CHANGE THE FOLLOWING
ThresholdImg thresholdImg;
ArrayList <Circle> circles = new ArrayList <Circle> ();
int timeout;
boolean canDraw, canFormat;

void setup () {
  size(360, 480);

  thresholdImg = new ThresholdImg(loadImage(IMAGE_NAME), IMAGE_THRESHOLD);
  circles.add(new Circle(new PVector(random(width), random(height)), 0));
}

void draw() {
  addCircle();

  if (timeout >= TIMEOUT_LIMIT) {
    formatData();
    timeout();
  } else {

    // GROWING
    for (Circle other : circles) {
      for (Circle current : circles) {
        if (!current.touching && current.center != other.center) { // Are you comparing a circle to itself?

          for (int angle = 0; angle < 360; angle++) { // Check if the edge of the current circle touches something above the threshold.
            float rad = radians(angle);
            int x = int(cos(rad)*current.radius + current.center.x);
            int y = int(sin(rad)*current.radius + current.center.y);

            if (!thresholdImg.isValid(x, y)) {
              current.touching = true;
              break;
            }
          }

          float distance = current.center.dist(other.center);
          float touchingDistance = current.radius + other.radius + STROKE_WEIGHT*2 - current.growStep - other.growStep;

          if (distance < touchingDistance) { // Check if this circle is touching another circle.
            current.touching = true;
          }
        }
      }
    }

    // DRAW CIRCLES
    //threshImg.display();
    background(WHITE);
    noFill();
    stroke(BLACK);
    strokeWeight(STROKE_WEIGHT);

    for (Circle c : circles) {
      c.update();
      c.display();
    }

    if (canFormat && timeout % FORMAT_FREQUENCY == 0) {
      formatData();
      saveSVG();
      canFormat = false;
    }
  }
}