/* 
 Sorts all Circle objects accoring to thier center in ascending order.
 
 If two Circles have the same primarySort (x), they will be assorted in ascending
 order by thier secondarySort (y).
 */
class CircleComparator implements Comparator {

  int compare(Object a, Object b) { 
    PVector centerA = ((Circle) a).center;
    PVector centerB = ((Circle) b).center;

    float primarySortA = centerA.x;
    float primarySortB = centerB.x;

    if (primarySortA < primarySortB) {
      return -1;
    } else {
      float secondarySortA = centerA.y;
      float secondarySortB = centerB.y;

      if (primarySortA == primarySortB) {
        return secondarySortA < secondarySortB ? -1 : 0;
      } else {
        return 1;
      }
    }
  }
}