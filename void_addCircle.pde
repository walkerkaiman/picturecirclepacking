void addCircle() {
  int randomIndex = int(random(thresholdImg.filteredPositions.size()-1));
  PVector randomPos = thresholdImg.filteredPositions.get(randomIndex);

  for (Circle c : circles) {
    float distance = c.center.dist(randomPos);
    float minDistance = c.radius - STROKE_WEIGHT * 2;

    if (distance == 0 || distance < minDistance) { // Too close to another circle
      canDraw = false;
      canFormat = true;
      timeout++;
      thresholdImg.filteredPositions.remove(randomIndex);
      println("Countdown: " + (TIMEOUT_LIMIT - timeout));
      break;
    }
    canDraw = true;
  }

  if (canDraw) {
    circles.add(new Circle(randomPos, 0));
  }
}