class Circle {
  PVector center;
  float radius, growStep;
  boolean touching;

  Circle(PVector center, float radius) {
    this.center = center;
    this.radius = radius;
    this.growStep = random(1);
    touching = false;
  }

  void update() {
    if (!touching) {
      //println(center + " is growing!");
      radius += growStep;
    }
  }

  void display() {
    stroke(BLACK);
    ellipse(center.x, center.y, radius*2, radius*2);
  }
}